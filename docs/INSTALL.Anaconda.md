# Detailed installation instructions for Anaconda

Here, Anaconda install and installation of _iocbio.sparks_ software is
described in details and assisted with the screenshots. The example
installation was done on Windows 10, but should be similar on other
platforms.

## Step 1

Download Anaconda with the support for Python 3.6 or higher. For that,

1. Go to http://anaconda.com
2. Go to Download page ![Click on download link](anaconda-install/download-1.png)
3. Select Python 3.6 or higher ![Select version](anaconda-install/download-2.png)

## Step 2

Install Anaconda as any regular program. Prefer to use default
options. When asked for installation of Microsoft VSCode, you can
choose to skip it since our software does not use it. Corresponding
screenshot below

![VSCode](anaconda-install/install-option.jpg).

## Step 3

After installation, start Anaconda Navigator from Start menu. Select
channels, as shown in Anaconda Navigator screenshot below.

![Navigator](anaconda-install/navigator-channels.png)

## Step 4

Add channels. For that,

1. Press Add... in channels dialog

![Add](anaconda-install/add-channels.png)

2. Add _conda-forge_ and _iocbio_ channels. For that, type in a name of a channel and press Enter, one channel per line, as shown below.

![Channels](anaconda-install/add-channels-2.jpg)

3. Update the channels by pressing "Update channels" after adding them.

![Update](anaconda-install/add-channels-3.png)

## Step 5

After channels update, you should see _iocbio.sparks_ on Anaconda Navigator workspace. Install it by clicking "Install" button.

![Install](anaconda-install/install.png)

## Step 6

To run the application, start it by pressing "Launch"

![Run](anaconda-install/run.png)


Consider making a shortcut to _iocbio.sparks_ to the startup script in
`Anaconda/Scripts` directory.